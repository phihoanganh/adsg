package com.adsg.api;
import com.adsg.model.model.AdsModel;
import com.adsg.model.model.AreaModel;
import com.adsg.network.net.ServerPath;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Streaming;
import retrofit2.http.Url;

public interface ServerRequest {

    @GET(ServerPath.API_GET_NEWS)
    Call<List<AdsModel>> getNews();

    @GET
    Call<AreaModel> getCurrentDistrict(@Url String url);

    @Streaming
    @GET
    Call<ResponseBody> downloadFileWithDynamicUrlSync(@Url String fileUrl);
}
