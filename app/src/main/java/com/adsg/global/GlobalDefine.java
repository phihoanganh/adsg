package com.adsg.global;

/**
 * Created by admin on 9/5/2016.
 */
public class GlobalDefine {
    //Common code Request
    public static final String REQUEST_STATUS_SUCCESS = "ok";
    public static final String REQUEST_STATUS_FAIL = "fail";
    public static final int REQUEST_CODE_FAIL = 0;
    public static int COUNT_ADS = 0;
    //Key save share preference
    public static final String KEY_SAVE_AUTH_TOKEN = "key_save_auth_token";//b9f2a8a18ec4b3bd7c9a5c7c7874a338
    public static final String KEY_SAVE_BONUS_CODE_TIME = "key_save_bonus_code_time";
    public static final String KEY_SAVE_DEVICE_ID = "key_save_device_id";
    public static final String KEY_AUTH_TOKEN_HEADER = "Auth-Token";
    public static final String KEY_AUTH_DEVICE_ID_HEADER = "X-deviceId";
    public static final String KEY_SAVE_USER_PROFILE = "key_save_user_profile";
    public static final String KEY_LANGUAGE_HEADER = "X-Language";
    public static final String KEY_SAVE_LANGUAGE_USER_SELECT = "key_save_language_user_select";

    public static final String KEY_BUNDLE_BASE_ITEM_INFO = "KEY_BUNDLE_BASE_ITEM_INFO";
    public static final String WEB_VIEW_TERM_CSS = "<html><head><meta name='viewport' content='initial-scale=1.0, maximum-scale=1.0, user-scalable=0'/><style>table{ border-collapse: collapse !important;border-spacing: 0 !important;width: 100% !important; border: 1px solid #ddd !important;} th, td { border: ridge !important; text-align: left !important;padding: 8px !important;} strong{margin-top:10px;} @font-face { font-family:myFirstFont; src:url(fonts/Roboto-Light.ttf); } img {max-width: 100%;height:inherit !important;} * {font-family: myFirstFont !important; font-size: 14px !important;line-height:1.4em !important;} a{font-size: 16px; color: #e1690d !important;} .mgift{font-size: 16px; color: #e1690d !important;} body{margin:8px;} strong{margin-top:8px;} </style></head><body><div>";
}
