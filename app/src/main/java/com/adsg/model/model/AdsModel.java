package com.adsg.model.model;

import android.location.Location;
import android.util.Log;

import com.adsg.realm.RealmController;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by anhnguyen on 11/1/16.
 */

public class AdsModel {
    private Boolean active;
    private Boolean adminApproval;
    private Boolean agencyApproval;
    private ArrayList<String> ages;
    private String description;
    private String id;
    private String name;
    private String url;
    private ArrayList<Zones> zones;
    private String fileName;
    private View view;
    private int bid;
    private MLocation location;
    private double limitationViews;
    private double limitationCostPerDay;
    private double points;
    private String modifiedBy;
    private int commissionA;
    private int commissionB;

    public Boolean getActive() {
        return active;
    }

    public Boolean getAdminApproval() {
        return adminApproval;
    }

    public Boolean getAgencyApproval() {
        return agencyApproval;
    }

    public ArrayList<String> getAges() {
        return ages;
    }

    public String getDescription() {
        return description;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }

    public View getView() {
        return view;
    }

    public int getBid() {
        return bid;
    }

    public MLocation getLocation() {
        return location;
    }

    public double getLimitationViews() {
        return limitationViews;
    }

    public double getLimitationCostPerDay() {
        return limitationCostPerDay;
    }

    public double getPoints() {
        return points;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public ArrayList<Zones> getZones() {
        return zones;
    }

    public int getCommissionA() {
        return commissionA;
    }

    public int getCommissionB() {
        return commissionB;
    }

    private class Zones {
        private String id;
        private String name;
        private Boolean active;

        public String getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public Boolean getActive() {
            return active;
        }
    }

    private class View {
        private double totalView;

        public double getTotalView() {
            return totalView;
        }
    }

    private class MLocation {
        @SerializedName("long")
        private double lng;
        private double lat;
        private double rad;

        public double getLng() {
            return lng;
        }

        public double getLat() {
            return lat;
        }

        public double getRad() {
            return rad;
        }
    }

    public String getFileName() {
        String fileName = null;
        if (url != null && url != "") {
            String[] ary = url.split("/");
            fileName = ary[ary.length - 1];
        }
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public double setPoints(double playedTime, double totalAds, Location currenLocation, String district) {
//        Log.d("CALLLLL*****", name);
        this.points = calBid() * calRp() * (playedTime / totalAds) * calTp(currenLocation, district);
//        Log.d("CALLLLL", "playedTime: " + playedTime + ", totalAds: " + totalAds);
//        Log.d("CALLLLL", "wp " + (playedTime / totalAds));
//        Log.d("CALLLLL", "point: " + this.points);
        return this.points;
    }

    private double calRp() {
        if (view == null) {
            return 1;
        }
//        Log.d("CALLLLL", "getTotalView: " + view.getTotalView() + ", limitationViews:" + limitationViews);
        if (limitationViews > 0 && view.getTotalView() < limitationViews) {
            return 1 - (view.getTotalView() / limitationViews);
        }
        return -1;
    }

    private int calBid() {
        if (this.bid == 0) return 1;
        return bid;
    }

    private double calTp(Location currenLocation, String district) {
        if (location != null && currenLocation != null && calDistance(currenLocation) <= location.getRad()) {
            return 2;
        }
        if (zones != null && zones.size() > 0) {
            for (Zones zone : zones) {
                if (district.equals(zone.getName())) {
                    return 1.5;
                }
            }
        }
        return 1;
    }

    private double calDistance(Location currenLocation) {
        double latDistance = Math.toRadians(currenLocation.getLatitude() - location.getLat());
        double lngDistance = Math.toRadians(currenLocation.getLongitude() - location.getLng());

        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(currenLocation.getLatitude())) * Math.cos(Math.toRadians(location.getLat()))
                * Math.sin(lngDistance / 2) * Math.sin(lngDistance / 2);

        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        return (Math.round(6371 * c));
    }
}