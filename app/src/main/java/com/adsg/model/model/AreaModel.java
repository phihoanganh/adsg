package com.adsg.model.model;

import android.location.Location;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anhnguyen on 11/1/16.
 */

public class AreaModel {
    private List<Result> results;

    public List<Result> getResults() {
        return results;
    }

    public class Result {
        private String display_name;
        private String name;

        public String getDisplay_name() {
            return display_name;
        }

        public String getName() {
            return name;
        }
    }
}