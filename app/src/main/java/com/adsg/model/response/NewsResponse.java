package com.adsg.model.response;


import com.adsg.model.model.AdsModel;

import java.util.ArrayList;

/**
 * Created by PC on 10/27/2016.
 */

public class NewsResponse {
    private ArrayList<AdsModel> result;

    public ArrayList<AdsModel> getResult() {
        return result;
    }
}