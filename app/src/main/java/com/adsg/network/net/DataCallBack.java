package com.adsg.network.net;

/**
 * Created by admin on 9/5/2016.
 */
public abstract class DataCallBack<T> {
    public abstract void onSuccess(T result);

    public void onError(int errorCode) {
    }
}
