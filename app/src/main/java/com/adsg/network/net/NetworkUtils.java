package com.adsg.network.net;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;


import java.io.IOException;

import com.adsg.api.ServerRequest;
import com.adsg.global.GlobalDefine;

import com.adsg.utils.SharedPreferencesUtils;
import com.adsg.utils.StringUtils;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by admin on 9/5/2016.
 */
public class NetworkUtils {

    private static NetworkUtils mInstance;
    private Context context;
    private Retrofit retrofit;
    private ServerRequest service;

    public NetworkUtils() {
    }

    public static synchronized NetworkUtils getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new NetworkUtils();
            mInstance.setContext(context);
        }

        return mInstance;
    }

    public static synchronized NetworkUtils getInstance() {
        if (mInstance == null) {
            try {
                throw new Exception("The getInstance() function have to call after getInstance(Context context) function.");
            } catch (Exception var1) {
                var1.printStackTrace();
            }
        }

        return mInstance;
    }

    public Context getContext() {
        return this.context;
    }

    public void setContext(Context context) {
        this.context = context;
    }


    public ServerRequest getRetrofitService() {
        return getRetrofitServiceWithDefaultHeader();
    }

    public static NetworkInfo getNetworkInfo(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo();
    }

    public static boolean isConnected(Context context) {
        NetworkInfo info = getNetworkInfo(context);
        return info != null && info.isConnected();
    }

    public ServerRequest getRetrofitServiceWithDefaultHeader() {
        if (null == service) {
            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
            httpClient.addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Interceptor.Chain chain) throws IOException {
                    Request original = chain.request();
                    Request request = original.newBuilder()
                            .header(GlobalDefine.KEY_AUTH_TOKEN_HEADER,
                                    !StringUtils.isNullOrEmpty(SharedPreferencesUtils.getInstance(getContext()).getString(GlobalDefine.KEY_SAVE_AUTH_TOKEN)) == false
                                            ? "" : SharedPreferencesUtils.getInstance(getContext()).getString(GlobalDefine.KEY_SAVE_AUTH_TOKEN))
                            .header(GlobalDefine.KEY_LANGUAGE_HEADER,
                                    !StringUtils.isNullOrEmpty(SharedPreferencesUtils.getInstance(getContext()).getString(GlobalDefine.KEY_SAVE_LANGUAGE_USER_SELECT)) == false
                                            ? "en_US" : SharedPreferencesUtils.getInstance(getContext()).getString(GlobalDefine.KEY_SAVE_LANGUAGE_USER_SELECT))
                            .header(GlobalDefine.KEY_AUTH_DEVICE_ID_HEADER,!StringUtils.isNullOrEmpty(SharedPreferencesUtils.getInstance(getContext()).getString(GlobalDefine.KEY_SAVE_DEVICE_ID)) == false
                                    ? "" :
                                    SharedPreferencesUtils.getInstance(getContext()).getString(GlobalDefine.KEY_SAVE_DEVICE_ID))
                            .method(original.method(), original.body())
                            .build();

                    return chain.proceed(request);
                }
            });

            OkHttpClient client = httpClient.build();
            retrofit = new Retrofit.Builder()
                    .baseUrl("https://stagingapi.com.dadconan.vn")//TODO
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build();
            service = retrofit.create(ServerRequest.class);
        }

        return service;
    }


}
