package com.adsg.network.repository;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.adsg.global.GlobalDefine;
import com.adsg.model.model.AdsModel;
import com.adsg.model.model.AreaModel;
import com.adsg.network.net.DataCallBack;
import com.adsg.network.net.NetworkUtils;
import com.adsg.network.net.ServerPath;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * OØ
 * Created by admin on 9/14/2016.
 */
public class HomeRepository extends ServerPath {
    private static HomeRepository mInstance;
    private Context context;

    public static synchronized HomeRepository getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new HomeRepository(context);
        }
        return mInstance;
    }

    public HomeRepository(Context context) {
        mInstance = this;
        this.context = context;
    }

    public void getNews(final DataCallBack<List<AdsModel>> callBack) {
        Call<List<AdsModel>> call = NetworkUtils.getInstance(context).getRetrofitService().getNews();
        call.enqueue(new Callback<List<AdsModel>>() {
            @Override
            public void onResponse(Call<List<AdsModel>> call, Response<List<AdsModel>> response) {
                callBack.onSuccess(response.body());
            }

            @Override
            public void onFailure(Call<List<AdsModel>> call, Throwable t) {
                callBack.onError(GlobalDefine.REQUEST_CODE_FAIL);
            }
        });
    }

    public void getCurrentDistrict(String location, final DataCallBack<AreaModel> callBack) {

        Log.d("AAAAAA", ServerPath.API_GET_AREA + location);
        Call<AreaModel> call = NetworkUtils.getInstance(context).getRetrofitService().getCurrentDistrict(ServerPath.API_GET_AREA + location);
        call.enqueue(new Callback<AreaModel>() {
            @Override
            public void onResponse(Call<AreaModel> call, Response<AreaModel> response) {
                callBack.onSuccess(response.body());
            }

            @Override
            public void onFailure(Call<AreaModel> call, Throwable t) {
                callBack.onError(GlobalDefine.REQUEST_CODE_FAIL);
            }
        });
    }

    public void downloadVideo(String fileUrl, String fileName, final DataCallBack<String> callBack) {
        Call<ResponseBody> call = NetworkUtils.getInstance(context).getRetrofitService().downloadFileWithDynamicUrlSync(fileUrl);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    new AsyncTask<Void, Void, String>() {
                        @Override
                        protected String doInBackground(Void... voids) {
                            String localPath = context.getExternalFilesDir(null) + File.separator + fileName;
                            Log.d("DownloadFile", "file write to : " + localPath);

                            boolean writtenToDisk = writeResponseBodyToDisk(response.body(), localPath);
                            if (writtenToDisk) {
                                return localPath;
                            }
                            Log.d("DownloadFile", "file download was a success? " + writtenToDisk);
                            return null;
                        }

                        protected void onPostExecute(String localPath) {
                            if (localPath != null)
                                callBack.onSuccess(localPath);
                            else
                                callBack.onError(GlobalDefine.REQUEST_CODE_FAIL);
                        }
                    }.execute();

                } else {
                    callBack.onError(GlobalDefine.REQUEST_CODE_FAIL);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                callBack.onError(GlobalDefine.REQUEST_CODE_FAIL);
            }
        });
    }

    private boolean writeResponseBodyToDisk(ResponseBody body, String localPath) {
        try {
            // todo change the file location/name according to your needs
            File futureStudioIconFile = new File(localPath);

            InputStream inputStream = null;
            OutputStream outputStream = null;

            try {
                byte[] fileReader = new byte[4096];

                long fileSize = body.contentLength();
                long fileSizeDownloaded = 0;

                inputStream = body.byteStream();
                outputStream = new FileOutputStream(futureStudioIconFile);

                while (true) {
                    int read = inputStream.read(fileReader);

                    if (read == -1) {
                        break;
                    }

                    outputStream.write(fileReader, 0, read);

                    fileSizeDownloaded += read;

                }
                outputStream.flush();
                return true;
            } catch (IOException e) {
                return false;
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }

                if (outputStream != null) {
                    outputStream.close();
                }
            }
        } catch (IOException e) {
            return false;
        }
    }
}
