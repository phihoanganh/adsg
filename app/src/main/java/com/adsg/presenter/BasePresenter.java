package com.adsg.presenter;

import android.content.Context;

import com.adsg.views.iface.IViewBase;

/**
 * Created by admin on 9/5/2016.
 */
public abstract class BasePresenter<I extends IViewBase> {

    private I iFace;

    public BasePresenter() {
    }

    public I getIFace() {
        return this.iFace;
    }

    public void setIFace(I iFace) {
        this.iFace = iFace;
    }

    protected Context getContext() {
        return this.iFace != null ? this.iFace.getContext() : null;
    }

    protected String getTag() {
        return this.getClass().getName();
    }

    public void onInit() {
    }


}
