package com.adsg.presenter;

import android.content.Context;
import android.util.Log;

import com.adsg.model.model.AdsModel;
import com.adsg.model.model.AreaModel;
import com.adsg.network.net.DataCallBack;
import com.adsg.network.repository.HomeRepository;
import com.adsg.views.iface.IHomeView;

import java.util.List;


/**
 * Created by anhnguyen on 9/23/16.
 */

public class HomePresenter extends BasePresenter<IHomeView> {
    private HomeRepository homeRepository;
    private Context context;

    @Override
    public void onInit() {
        super.onInit();
        homeRepository = new HomeRepository(getIFace().getContext());
    }

    public void getAds() {
        homeRepository.getNews(new DataCallBack<List<AdsModel>>() {
            @Override
            public void onSuccess(List<AdsModel> result) {
                getIFace().getNewsSuccess(result);
            }

            @Override
            public void onError(int errorCode) {
                getIFace().getNewsError(errorCode);
            }
        });
    }

    public void getCurrentDistrict(String location) {
        homeRepository.getCurrentDistrict(location, new DataCallBack<AreaModel>() {
            @Override
            public void onSuccess(AreaModel result) {
                getIFace().getCurrentDistrictSuccess(result);
            }

            @Override
            public void onError(int errorCode) {
                getIFace().getCurrentDistrictError(errorCode);
            }
        });
    }

    public void downloadVideos(String url, String fileName) {
        Log.d("Call New", "Begin 1");
        homeRepository.downloadVideo(url, fileName, new DataCallBack<String>() {
            @Override
            public void onSuccess(String result) {
                getIFace().downloadVideoSuccess(result);
            }

            @Override
            public void onError(int errorCode) {
                getIFace().downloadVideoError(errorCode);
            }
        });
    }

    public void downloadOneVideo(String url, String fileName) {
        Log.d("Call New", "Begin 1");
        homeRepository.downloadVideo(url, fileName, new DataCallBack<String>() {
            @Override
            public void onSuccess(String result) {
                getIFace().downloadOneVideoSuccess(result);
            }

            @Override
            public void onError(int errorCode) {
                getIFace().downloadOneVideoError(errorCode);
            }
        });
    }
}
