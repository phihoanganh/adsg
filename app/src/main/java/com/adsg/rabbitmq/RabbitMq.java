package com.adsg.rabbitmq;

import android.util.Log;

import com.rabbitmq.client.AlreadyClosedException;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeoutException;

/**
 * Created by MacBookPro on 9/4/18.
 */

public class RabbitMq {
    private ConnectionFactory factory;
    private Channel channel = null;
    private Thread thread;
    public static final String EXCHANGE_PLAY_VIDEO = "msX.Exchange.msx-adsg.mobile.info";
    public static final String EXCHANGE_GPS = "msX.Exchange.msx-adsg.mobile.gps";
    private BlockingDeque<List<String>> queue = new LinkedBlockingDeque<>();


    public RabbitMq() {
        setupRabbit();
    }

    private void setupRabbit() {
        factory = new ConnectionFactory();
        factory.setHost("52.221.240.243");
        factory.setPort(5672);
        factory.setUsername("rabbitmq");
        factory.setPassword("rabbitmq");
        if (channel != null) return;
//        new ConnectRabbitTask().execute();
        thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Connection connection = null;
                    connection = factory.newConnection();
                    channel = connection.createChannel();
                    // Listen from server side
//                    String queueName = channel.queueDeclare().getQueue();
//                    channel.queueBind(queueName, EXCHANGE_PLAY_VIDEO, "");
//                    Consumer consumer = new DefaultConsumer(channel) {
//                        @Override
//                        public void handleDelivery(String consumerTag, Envelope envelope,
//                                                   AMQP.BasicProperties properties, byte[] body) throws IOException {
//                            String message = new String(body, "UTF-8");
//                            Log.d("Rabbitmq", " Received '" + envelope.getRoutingKey() + "':'" + message + "'");
//                        }
//                    };
//                    channel.basicConsume(queueName, true, consumer);
                    //End listener
                    channel.exchangeDeclare(EXCHANGE_PLAY_VIDEO, "fanout");
                    while (true) {
                        try {
                            List<String> message = queue.takeFirst();
                            if (!message.isEmpty() && message.size() > 1) {
                                Log.d("Rabbitmq", " Sent '" + message.get(1) + "':'" + message.get(0) + "'");
                                channel.basicPublish(message.get(1), "", null, message.get(0).getBytes("UTF-8"));
                            }
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        } catch (AlreadyClosedException e) {
                            thread.interrupt();
                            setupRabbit();
                        }
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (TimeoutException e) {
                    e.printStackTrace();
                }
            }
        });

        thread.start();
//        channel.close();
//        connection.close();
    }
    public void clearQueue(){
        queue.clear();
    }
    public void interrupt(){
        if (thread != null) {
            thread.interrupt();
        }
    }
    public void publishMessage(String message, String exchangeS){
        try {
            List<String> temp = new ArrayList<>();
            temp.add(message);
            temp.add(exchangeS);
            queue.putLast(temp);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
