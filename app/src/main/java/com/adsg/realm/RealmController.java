package com.adsg.realm;


import android.app.Activity;
import android.app.Application;
import android.support.v4.app.Fragment;

import com.adsg.model.model.LocalPathModel;
import com.adsg.model.model.ViewCountModel;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;


public class RealmController {

    private static RealmController instance;
    private final Realm realm;

    public RealmController(Application application) {
        Realm.init(application.getApplicationContext());
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
                .name(Realm.DEFAULT_REALM_NAME)
                .schemaVersion(0)
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(realmConfiguration);
        realm = Realm.getDefaultInstance();
    }

    public static RealmController with(Fragment fragment) {

        if (instance == null) {
            instance = new RealmController(fragment.getActivity().getApplication());
        }
        return instance;
    }

    public static RealmController with(Activity activity) {

        if (instance == null) {
            instance = new RealmController(activity.getApplication());
        }
        return instance;
    }

    public static RealmController with(Application application) {

        if (instance == null) {
            instance = new RealmController(application);
        }
        return instance;
    }

    public static RealmController getInstance() {

        return instance;
    }

    public Realm getRealm() {
        return realm;
    }

    //Refresh the realm istance
    public void refresh() {
        realm.refresh();
    }

    ////////////////////// LOCAL FILE ////////////////////
    public void addAPath(String fileName, String result) {
        LocalPathModel localPathModel = new LocalPathModel();
        localPathModel.setFileName(fileName);
        localPathModel.setFilePath(result);
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(localPathModel);
        realm.commitTransaction();
    }

    public void clearAllLocalPath() {
        realm.beginTransaction();
        realm.delete(LocalPathModel.class);
        realm.commitTransaction();
    }

    //find all objects in the Book.class
    public RealmResults<LocalPathModel> getLocalPaths() {

        return realm.where(LocalPathModel.class).findAll();
    }

    //query a single item with the given id
    public LocalPathModel getLocalPathByName(String fileName) {
        return realm.where(LocalPathModel.class).equalTo("fileName", fileName).findFirst();
    }
    ////////////////////// END LOCAL FILE ////////////////////

    ////////////////////// VIEW COUNT ////////////////////
    public void addOUpdateViewCount(String id, int view) {
        ViewCountModel viewCountModel = new ViewCountModel();
        viewCountModel.setId(id);
        viewCountModel.setViewCount(view);
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(viewCountModel);
        realm.commitTransaction();
    }

    public void updateViewCount(String id, int videoListSize) {
        RealmResults<ViewCountModel> viewCountModels = getAllViewCount();
        boolean updateExistItem = false;
        for (ViewCountModel viewCountModel : viewCountModels) {
            if (viewCountModel.getId().equals(id)) {
                updateExistItem = true;
                addOUpdateViewCount(id, 0);
            } else if (viewCountModel.getViewCount() + 2 >= videoListSize) {
                removeAViewCount(viewCountModel);
            } else {
                addOUpdateViewCount(viewCountModel.getId(), viewCountModel.getViewCount() + 1);
            }
        }
        if (!updateExistItem)
            addOUpdateViewCount(id, 0);

    }

    public void removeAViewCount(ViewCountModel viewCountModel) {
        realm.beginTransaction();
        viewCountModel.deleteFromRealm();
        realm.commitTransaction();
    }

    public void clearAllViewCount() {
        realm.beginTransaction();
        realm.delete(ViewCountModel.class);
        realm.commitTransaction();
    }

    public RealmResults<ViewCountModel> getAllViewCount() {
        return realm.where(ViewCountModel.class).findAll();
    }

    //query a single item with the given id
    public ViewCountModel getViewCountById(String id) {
        return realm.where(ViewCountModel.class).equalTo("id", id).findFirst();
    }
    ////////////////////// END COUNT ////////////////////
}
