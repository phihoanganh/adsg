package com.adsg.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by agree on 9/24/17.
 */

public class DateUtils {
    private static DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static DateFormat fmt = new SimpleDateFormat("MMMM dd, yyyy");

    public static String formatDateMothsName(String date) {
        Date startDate;
        try {
            startDate = df.parse(date);
            String newDateString = fmt.format(startDate);
            return newDateString;
        } catch (ParseException e) {
            return date;
        }
    }
}
