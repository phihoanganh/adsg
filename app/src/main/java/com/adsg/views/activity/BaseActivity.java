package com.adsg.views.activity;

import android.app.Dialog;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.adsg.R;
import com.adsg.presenter.BasePresenter;
import com.adsg.views.iface.IViewBase;

import java.lang.reflect.ParameterizedType;

/**
 * Created by admin on 9/5/2016.
 */
public class BaseActivity<P extends BasePresenter> extends AppCompatActivity {//implements RewardedVideoAdListener
    private P viewPresenter;
    private Dialog progressDialog;

    public P getPresenter(IViewBase iViewBase) {
        try {
            if (this.viewPresenter == null) {
                String e = ((Class) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[0]).getName();
                Class classDefinition = Class.forName(e);
                this.viewPresenter = (P) classDefinition.newInstance();
                this.viewPresenter.setIFace(iViewBase);
                this.viewPresenter.onInit();
                return this.viewPresenter;
            }
        } catch (InstantiationException var4) {
            var4.printStackTrace();
        } catch (IllegalAccessException var5) {
            var5.printStackTrace();
        } catch (ClassNotFoundException var6) {
            var6.printStackTrace();
        }

        return this.viewPresenter;
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE); // Make to run your application only in portrait mode
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
    }

    public void showDialogNotify(String message) {
        final Dialog dlNotify = new Dialog(this);
        dlNotify.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dlNotify.setContentView(R.layout.v3_dialog_notify);
        TextView tvMessage = dlNotify.findViewById(R.id.tvMessage);
        Button btnClose = dlNotify.findViewById(R.id.btnClose);
        tvMessage.setText(message);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dlNotify.dismiss();
            }
        });
        dlNotify.show();
    }

    public void showDialogInternetConnectionUnstable() {
        showDialogNotify(getString(R.string.msg_unstable_connection));
    }

    public void showProgressDialog() {
        if (null == progressDialog) {
            progressDialog = new Dialog(this, R.style.DialogCustomTheme);
            progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            progressDialog.setContentView(R.layout.progress_dialog);
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(progressDialog.getWindow().getAttributes());
            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            progressDialog.getWindow().setAttributes(lp);
            progressDialog.setCanceledOnTouchOutside(false);
        }
        if (!progressDialog.isShowing())
            progressDialog.show();

    }

    public void dismissProgressDialog() {
        if (null != progressDialog && progressDialog.isShowing())
            progressDialog.dismiss();
    }
public void loadRewardedVideoAd(){}
    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
