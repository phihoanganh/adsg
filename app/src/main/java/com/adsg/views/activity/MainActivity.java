package com.adsg.views.activity;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;

import com.adsg.R;
import com.adsg.model.model.AdsModel;
import com.adsg.model.model.AreaModel;
import com.adsg.model.model.LocalPathModel;
import com.adsg.model.model.ViewCountModel;
import com.adsg.presenter.HomePresenter;
import com.adsg.rabbitmq.RabbitMq;
import com.adsg.realm.RealmController;
import com.adsg.views.iface.IHomeView;
import com.halilibo.bettervideoplayer.BetterVideoCallback;
import com.halilibo.bettervideoplayer.BetterVideoPlayer;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.List;

import io.realm.Realm;

public class MainActivity extends BaseActivity<HomePresenter> implements IHomeView, BetterVideoCallback {

    private BetterVideoPlayer vidView;
    private int downloadIndex = 0;
    private static List<AdsModel> videoList;
    private AdsModel lastViewAds = null;
    private String deviceId = "Can't Get ID";
    private String locationProvider = LocationManager.NETWORK_PROVIDER;
    private LocationManager locationManager;
    private int TAG_CODE_PERMISSION_LOCATION = 100;
    private Boolean videosDownloading = false;
    private Location lastLocation;
    private RabbitMq rabbitMq;
    private String currentZone = "";
    private Boolean isLoadingAds = false;
    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        deviceId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        requestGPS();
        setUpRealm();
        settingVideoView();
        rabbitMq = new RabbitMq();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (videoList != null && !videoList.isEmpty() && videoList.size() > 0) {
            playNextVideo();
        } else {
            if (lastLocation != null)
                getListAds();
        }
    }

    //////////////////////// GPS////////////////////////////

    private void requestGPS() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION},
                    TAG_CODE_PERMISSION_LOCATION);
        } else {
            locationManager.requestLocationUpdates(locationProvider, 10, 1, locationListener);
            lastLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        }
    }

    LocationListener locationListener = new LocationListener() {
        public void onLocationChanged(Location location) {
            // Called when a new location is found by the network location provider.
            if ((videoList == null || videoList.isEmpty() || videoList.size() <= 0) && lastLocation == null) {
                getListAds();
            }
            lastLocation = location;
            try {
                JSONObject json = new JSONObject();
                json.put("lat", location.getLatitude());
                json.put("lng", location.getLongitude());
                json.put("deviceId", deviceId);
                String message = json.toString();
                rabbitMq.publishMessage(message, rabbitMq.EXCHANGE_GPS);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onProviderDisabled(String provider) {
        }
    };

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == TAG_CODE_PERMISSION_LOCATION) {
            requestGPS();
//            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                locationManager.requestLocationUpdates(locationProvider, 3000, 0, locationListener);
//                lastLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
//            } else {
//                ActivityCompat.requestPermissions(this, new String[]{
//                                Manifest.permission.ACCESS_FINE_LOCATION,
//                                Manifest.permission.ACCESS_COARSE_LOCATION},
//                        TAG_CODE_PERMISSION_LOCATION);
//            }
        }
    }
    //////////////////////// GPS////////////////////////////

    private void setUpRealm() {
        this.realm = RealmController.with(this).getRealm();
        RealmController.with(this).refresh();
    }

    private void settingVideoView() {
        vidView = findViewById(R.id.myVideo);
        vidView.setHideControlsOnPlay(true);
        vidView.setAutoPlay(true);
        vidView.hideControls();
        vidView.disableControls();
        vidView.setCallback(this);
//        vidView.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View view, MotionEvent motionEvent) {
//                Log.d("VIDEOVIEW1", "Touch");
//                return false;
//            }
//        });
    }

    @Override
    public void onPause() {
        super.onPause();
        // Make sure the player stops playing if the user presses the home button.
        vidView.pause();
    }

    @Override
    protected void onStop() {
        rabbitMq.clearQueue();
        if (lastLocation != null)
            rabbitMq.publishMessage(prepareMessage(lastViewAds, false), rabbitMq.EXCHANGE_PLAY_VIDEO);
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        rabbitMq.interrupt();
    }

    @Override
    public Context getContext() {
        return this;
    }

    /////////////////////////// START CALLBACK DOWNLOAD ADS AND PLAY VIDEO ///////////////
    private void getListAds() {
        if (isLoadingAds) return;
        isLoadingAds = true;
        getPresenter(this).getAds();
        if (lastLocation != null) {
            getPresenter(this).getCurrentDistrict(lastLocation.getLongitude() + "/" + lastLocation.getLatitude() + ".js");
        }
    }

    private void playNextVideo() {
        int videoListSize = videoList.size();
        double maxPointTemp = -1;
        AdsModel nextVideoTemp = null;
        for (AdsModel adsModel : videoList) {
            if (adsModel.getUrl() != null && adsModel.getUrl() != "") {
                ViewCountModel viewCountItem = RealmController.with(this).getViewCountById(adsModel.getId());
                int viewCountTemp = videoListSize;
                if (viewCountItem != null) {
                    viewCountTemp = viewCountItem.getViewCount();
                }
                double needCheckPoint = adsModel.setPoints(viewCountTemp, videoListSize, lastLocation, currentZone);
                if (needCheckPoint > maxPointTemp) {
                    maxPointTemp = needCheckPoint;
                    nextVideoTemp = adsModel;
                }
            }
        }
        if (nextVideoTemp == null) {
            if (lastLocation != null && lastViewAds != null) {
                rabbitMq.publishMessage(prepareMessage(lastViewAds, false), rabbitMq.EXCHANGE_PLAY_VIDEO);
                lastViewAds = null;
                getListAds();
            }
            return;
        }

        startNewVideo(nextVideoTemp.getUrl(), nextVideoTemp.getFileName());
        RealmController.with(this).updateViewCount(nextVideoTemp.getId(), videoListSize);
        if (lastLocation != null) {
            if (lastViewAds != null) {
                rabbitMq.publishMessage(prepareMessage(lastViewAds, false), rabbitMq.EXCHANGE_PLAY_VIDEO);
            }
            rabbitMq.publishMessage(prepareMessage(nextVideoTemp, true), rabbitMq.EXCHANGE_PLAY_VIDEO);
        }
        lastViewAds = nextVideoTemp;
    }


    private void startNewVideo(String vidAddress, String fileName) {
        Uri vidUri;
        LocalPathModel localPathModel = RealmController.with(this).getLocalPathByName(fileName);
        if (localPathModel != null) {
            File file = new File(localPathModel.getFilePath());
            if (file.exists()) {
                vidUri = Uri.fromFile(file);
            } else {
                //Download only one file and play video online
                vidUri = Uri.parse(vidAddress);
                getPresenter(this).downloadOneVideo(vidAddress, fileName);
            }
        } else {
            vidUri = Uri.parse(vidAddress);
            getPresenter(this).downloadOneVideo(vidAddress, fileName);
        }

        if (vidView != null) {
            vidView.reset();
            vidView.setSource(vidUri);
        }
    }

    private String prepareMessage(AdsModel adsModel, Boolean playStatus) {
        String message = "";
        try {
            JSONObject json = new JSONObject();
            json.put("name", adsModel.getName());
            json.put("campaignId", adsModel.getId());
            json.put("deviceId", deviceId);
            json.put("playStatus", playStatus);
            json.put("lat", lastLocation.getLatitude());
            json.put("lng", lastLocation.getLongitude());
            json.put("bid", adsModel.getBid());
            json.put("limitationCostPerDay", adsModel.getLimitationCostPerDay());
            json.put("limitationViews", adsModel.getLimitationViews());
            json.put("limitationViews", adsModel.getLimitationViews());
            json.put("modifiedBy", adsModel.getModifiedBy());
            json.put("commissionA", adsModel.getCommissionA());
            json.put("commissionB", adsModel.getCommissionB());

            message = json.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return message;
    }

    @Override
    public void getNewsError(int errorCode) {
        isLoadingAds = false;
        if (videoList != null) {
            return;
        }
        getListAds();
    }

    @Override
    public void getNewsSuccess(List<AdsModel> result) {
        isLoadingAds = false;
        if (result != null && !result.isEmpty() && result.size() > 0) {
            videoList = result;
            downloadNextVideo();
            if (lastViewAds == null)
                playNextVideo();
            return;
        }
        if (videoList != null) {
            return;
        }
        getListAds();
    }
    /////////////////////////// END CALLBACK DOWNLOAD ADS AND PLAY VIDEO ///////////////


    ////////////////// START DOWNLOAD VIDEO BLOCK///////////////////
    private void downloadNextVideo() {
        if (!(downloadIndex < videoList.size())) {
            downloadIndex = 0;
            return;
        }
        if (videosDownloading) return;
        videosDownloading = true;
        String fileUrl = videoList.get(downloadIndex).getUrl();
        if (fileUrl != null && fileUrl != "") {
            LocalPathModel localPathModel = RealmController.with(this).getLocalPathByName(videoList.get(downloadIndex).getFileName());
            if (localPathModel == null) {
                getPresenter(this).downloadVideos(videoList.get(downloadIndex).getUrl(), videoList.get(downloadIndex).getFileName());
            } else {
                continueDownloadVideo();
            }
        } else {
            continueDownloadVideo();
        }
    }

    private void continueDownloadVideo() {
        downloadIndex++;
        videosDownloading = false;
        downloadNextVideo();
    }

    @Override
    public void downloadVideoError(int errorCode) {
        continueDownloadVideo();
    }

    @Override
    public void downloadVideoSuccess(String result) {
        String[] ary = result.split("/");
        String fileName = ary[ary.length - 1];
        RealmController.with(this).addAPath(fileName, result);
        continueDownloadVideo();
    }

    @Override
    public void downloadOneVideoError(int errorCode) {

    }

    @Override
    public void downloadOneVideoSuccess(String result) {
        String[] ary = result.split("/");
        String fileName = ary[ary.length - 1];
        RealmController.with(this).addAPath(fileName, result);
    }
    ////////////////// END DOWNLOAD VIDEO BLOCK///////////////////

    ////////////////// START CALLBACK CURRENT DISTRICT BLOCK///////////////////
    @Override
    public void getCurrentDistrictError(int errorCode) {

    }

    @Override
    public void getCurrentDistrictSuccess(AreaModel result) {
        if (result != null && result.getResults() != null && result.getResults().size() > 0)
            currentZone = result.getResults().get(0).getName();
    }

    ////////////////// END CALLBACK CURRENT DISTRICT BLOCK///////////////////


    /////////////START VIDEO CALLBACK LISTENER////////////////////
    @Override
    public void onBackPressed() {
        return;
    }

    @Override
    public void onStarted(BetterVideoPlayer player) {

    }

    @Override
    public void onPaused(BetterVideoPlayer player) {

    }

    @Override
    public void onPreparing(BetterVideoPlayer player) {

    }

    @Override
    public void onPrepared(BetterVideoPlayer player) {

    }

    @Override
    public void onBuffering(int percent) {

    }

    @Override
    public void onError(BetterVideoPlayer player, Exception e) {
        playNextVideo();
        getListAds();
    }

    @Override
    public void onCompletion(BetterVideoPlayer player) {
        playNextVideo();
        getListAds();
    }

    @Override
    public void onToggleControls(BetterVideoPlayer player, boolean isShowing) {

    }
    /////////////END VIDEO CALLBACK LISTENER////////////////////

}
