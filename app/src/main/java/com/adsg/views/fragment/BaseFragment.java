package com.adsg.views.fragment;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.adsg.R;
import com.adsg.global.GlobalDefine;
import com.adsg.presenter.BasePresenter;
import com.adsg.views.iface.IViewBase;

import java.lang.reflect.ParameterizedType;

/**
 * Created by admin on 9/5/2016.
 */
public class BaseFragment<P extends BasePresenter> extends Fragment {

    private P viewPresenter;
    private Dialog progressDialog;
//    private com.facebook.ads.InterstitialAd interstitialAd;

    public BaseFragment() {

    }

    protected P getPresenter(IViewBase iface) {
        try {
            if (this.viewPresenter == null) {
                String e = ((Class) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[0]).getName();
                Class classDefinition = Class.forName(e);
                this.viewPresenter = (P) classDefinition.newInstance();
                this.viewPresenter.setIFace(iface);
                this.viewPresenter.onInit();
                return this.viewPresenter;
            }
        } catch (InstantiationException var4) {
            var4.printStackTrace();
        } catch (IllegalAccessException var5) {
            var5.printStackTrace();
        } catch (ClassNotFoundException var6) {
            var6.printStackTrace();
        } catch (java.lang.InstantiationException var7) {
            var7.printStackTrace();
        }

        return this.viewPresenter;
    }

    public void showDialogInternetConnectionUnstable() {
        showDialogNotify(getString(R.string.msg_unstable_connection));
    }

    public void showDialogNotify(String message) {
        final Dialog dlNotify = new Dialog(getContext());
        dlNotify.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dlNotify.setContentView(R.layout.v3_dialog_notify);
        TextView tvMessage = dlNotify.findViewById(R.id.tvMessage);
        Button btnClose = dlNotify.findViewById(R.id.btnClose);
        tvMessage.setText(message);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dlNotify.dismiss();
            }
        });
        dlNotify.show();
    }

    public void showProgressDialog() {
        if (null == progressDialog) {
            progressDialog = new Dialog(getContext(), R.style.DialogCustomTheme);
            progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            progressDialog.setContentView(R.layout.progress_dialog);
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(progressDialog.getWindow().getAttributes());
            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            progressDialog.getWindow().setAttributes(lp);
            progressDialog.setCanceledOnTouchOutside(false);
        }
        if (!progressDialog.isShowing())
            progressDialog.show();
    }

    public void dismissProgressDialog() {
        if (null != progressDialog && progressDialog.isShowing())
            progressDialog.dismiss();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
