package com.adsg.views.iface;

import com.adsg.model.model.AdsModel;
import com.adsg.model.model.AreaModel;

import java.util.List;

/**
 * Created by anhnguyen on 9/23/16.
 */

public interface IHomeView extends IViewBase {
    void getNewsError(int errorCode);

    void getNewsSuccess(List<AdsModel> result);

    void downloadVideoError(int errorCode);

    void downloadVideoSuccess(String result);

    void downloadOneVideoError(int errorCode);

    void downloadOneVideoSuccess(String result);

    void getCurrentDistrictError(int errorCode);

    void getCurrentDistrictSuccess(AreaModel result);
}
