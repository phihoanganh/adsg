package com.adsg.views.iface;

import android.content.Context;

/**
 * Created by admin on 9/5/2016.
 */
public interface IViewBase {
    Context getContext();
}
